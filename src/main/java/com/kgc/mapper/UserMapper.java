package com.kgc.mapper;

import com.kgc.entity.ShippingAddress;
import com.kgc.entity.ShoppingCar;
import com.kgc.entity.User;
import com.kgc.entity.UserRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {

    /**
     * 根据用户名和密码查询用户
     * @return
     */
    User queryByNameAndPassword(@Param("tel")String tel,@Param("password")String password);

    /**
     * 根据手机号查询用户
     * @return
     */
    User queryByTel(@Param("tel")String tel);

    /**
     * 注册
     * @param user
     * @return
     */
    int register(@Param("user") User user);

    /**
     * 查询所有用户信息并分页
     * @return
     */
    List<User> queryUser();

    /**
     * 查询所有的用户权限
     * @return
     */
    List<UserRole> queryUserRole();

    /**
     * 增加用户
     * @return
     */
    int adduser(@Param("user")User user);

    /**
     * 通过id删除该用户
     * @param id
     * @return
     */
    int deluser(@Param("id")int id);

    /**
     * 通过id查找该用户
     * @param id
     * @return
     */
    User queryUserById(@Param("id")int id);

    /**
     * 通过id更新该用户信息
     * @param user
     * @return
     */
    int updateuser(@Param("user")User user);


    /**
     * 修改密码
     * @param id
     * @param password
     * @return
     */
    int updatePassword(@Param("id")int id,@Param("password") String password);

    /**
     * 根据用户id查询收货地址
     * @param userid
     * @return
     */
    List<ShippingAddress> queryAddress(@Param("userid")int userid);


    /**
     * 完善或修改信息
     * @param user
     * @return
     */
    int completeInfo(@Param("user") User user);

    /**
     * 通过关键词查询User
     * @param searchUser
     * @return
     */
    List<User> admin_searchUser(@Param("searchUser")String searchUser);

    /**
     * 更改头像
     * @param headPath
     * @return
     */
    int updateHead(@Param("headPath") String headPath,@Param("id") int id);

    /**
     * 根据手机获取id
     * @param tel
     * @return
     */
    User queryIdByTel(@Param("tel") String tel);



    /**
     * 通过用户id获取订单图书详情
     * @param userId
     * @return
     */
    List<ShoppingCar> queryShopByUserId(@Param("userId") int userId);


    /**
     * 加入收藏
     * @param userId
     * @param bookId
     * @return
     */
    int  addFavorite(@Param("userId") int userId,@Param("bookId")int bookId);

    /**
     * 删除收藏
     * @param userId
     * @param bookId
     * @return
     */
    int  delFavorite(@Param("userId") int userId,@Param("bookId")int bookId);

    /**
     * 查询收藏
     * @param userId
     * @return
     */
    List<Integer>  queryFavorite(@Param("userId")int userId);


    /**
     * 确认收藏
     * @param userId
     * @param bookId
     * @return
     */
    int confirmFavorite(@Param("userId") int userId,@Param("bookId")int bookId);

    /**
     * 查询所有的手机号
     * @return
     */
    List<String> queryTel();

    /**
     * 删除购物车图书
     * @param userId
     * @param id
     * @return
     */
    int  delShopCarBook(@Param("userId") int userId,@Param("id")int id);


    /**
     * 加入购物车
     * @param shoppingCar
     * @return
     */
    int addShopCar(@Param("shoppingCar") ShoppingCar shoppingCar);

    /**
     * 通过用户id和图书id判断图书是否重复
     * @param userId
     * @param bookId
     * @return
     */
    int queryShopCarByUseridAndBookid(@Param("userId") int userId,@Param("bookId") int bookId);
}
