package com.kgc.service;

import com.kgc.entity.Comment;

import java.util.List;

public interface CommentService {
    //根据bookid查询评论
    List<Comment> queryByid(int bookid);
    //添加评论
    boolean add(Comment comment);
    //删除评论
    boolean del(int id);
}
