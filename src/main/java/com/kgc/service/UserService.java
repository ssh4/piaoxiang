package com.kgc.service;

import com.kgc.entity.ShippingAddress;
import com.kgc.entity.ShoppingCar;
import com.kgc.entity.User;
import com.kgc.entity.UserRole;

import java.util.List;

public interface UserService {
    /**
     * 根据用户名和密码查询用户
     * @return
     */
    User queryByNameAndPassword(String tel, String password);

    /**
     * 根据用户名和密码查询用户
     * @return
     */
    User queryByTel(String tel);

    /**
     * 注册
     * @param user
     * @return
     */
    int register( User user);

    /**
     * 查询所有用户信息
     * @return
     */
    List<User> queryUser();

    /**
     * 查询所有的用户分类
     * @return
     */
    List<UserRole> queryUserRole();

    /**
     * 后台增加用户
     * @param user
     * @return
     */
    int adduser(User user);

    /**
     * 通过id删除该条用户
     * @param id
     * @return
     */
    int deluser(int id);

    /**
     * 通过id查找对应用户信息
     * @param id
     * @return
     */
    User queryUserById(int id);

    /**
     * 通过id更新该用户
     * @param user
     * @return
     */
    int updateUser(User user);

    /**
     * 完善或修改信息
     * @param user
     * @return
     */
    int completeInfo(User user);

    /**
     * 根据用户id查询收货地址
     * @return
     */
    List<ShippingAddress> queryAddress(int userid);

    /**
     * 修改密码
     * @param id
     * @param password
     * @return
     */
    int updatePassword(int id,String password);

    /**
     * 通过关键词查询用户
     * @param searchUser
     * @return
     */
    List<User> admin_searchUser(String searchUser);

    /**
     * 修改头像
     * @param headPath
     * @param id
     * @return
     */
    boolean updateHead(String headPath,int id);

    /**
     * tel查询id
     * @param tel
     * @return
     */
    User queryIdByTel(String tel);


    /**
     * 通过用户id获取订单图书详情
     * @param userId
     * @return
     */
    List<ShoppingCar> queryShopByUserId(int userId);


    /**
     * 加入收藏
     * @param userId
     * @param bookId
     * @return
     */
    int  addFavorite(int userId,int bookId);

    /**
     * 删除收藏
     * @param userId
     * @param bookId
     * @return
     */
    int delFavorite(int userId,int bookId);

    /**
     * 查询收藏
     * @param userId
     * @return
     */
    List<Integer>  queryFavorite(int userId);

    /**
     * 确认收藏
     * @param userId
     * @param bookId
     * @return
     */
    boolean confirmFavorite(int userId,int bookId);

    /**
     * 查询所有的手机号
     * @return
     */
    List<String> queryTel();


    /**
     * 删除购物车图书
     * @param userId
     * @param id
     * @return
     */
    int delShopCarBook(int userId,int id);

    /**
     * 增加购物车
     * @param shoppingCar
     * @return
     */
    int addShoppingCar(ShoppingCar shoppingCar);

    /**
     * 通过用户id和图书id判断图书是否重复
     * @param userId
     * @param bookId
     * @return
     */
    int queryShopCarByUseridAndBookid(int userId, int bookId);
}
