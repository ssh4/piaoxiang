package com.kgc.service;

import com.kgc.entity.Order;

import java.util.List;

public interface OrderService {
    //增加一条订单信息
    int addOrder(Order order);
    //更新订单状态
    int updataOrderType(Integer orderType,String ordernumber);

    /**
     * 查询订单
     * @param userId
     * @return
     */
    List<Order> queryOrder(int userId);

    /**
     * 分类查询订单
     * @param userId
     * @param orderType
     * @return
     */
    List<Order> queryOrderByType( int userId,int orderType);


    List<Order> queryOrderByOrderNumber(String OrderNumber);

    int delOrder(int id);

}
