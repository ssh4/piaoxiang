package com.kgc.service.impl;

import com.kgc.entity.Comment;
import com.kgc.mapper.CommentMapper;
import com.kgc.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentMapper commentMapper;
    @Override
    public List<Comment> queryByid(int bookid) {
        return commentMapper.queryByid(bookid);
    }

    @Override
    public boolean add(Comment comment) {
        return commentMapper.add(comment)==1?true:false;
    }
    @Override
    public boolean del(int id){
        return commentMapper.del(id)==1?true:false;
    }

}
