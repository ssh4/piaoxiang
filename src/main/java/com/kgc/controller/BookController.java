package com.kgc.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.kgc.entity.Book;
import com.kgc.entity.BookType;
import com.kgc.entity.ShoppingCar;
import com.kgc.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("book")
@ResponseBody
public class BookController {

   @Autowired
   private BookService bookService;

    /**
     * 查询所有图书，并分页
     * @param pn
     * @param ps
     * @return
     */
    @RequestMapping("selectAll")
    public PageInfo queryAll(@RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             @RequestParam(value = "ps", defaultValue = "10") Integer ps){
        PageHelper.startPage(pn,ps);
        List<Book> books = bookService.queryBook();
        PageInfo info = new PageInfo(books,5);
        return info;
    }

    /**
     * 增加图书
     * @param book
     * @return
     */
    @RequestMapping("addbook")
    public boolean addBook(Book book){
        if (bookService.addBook(book)>0){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 查询所有图书分类
     * @return
     */
    @RequestMapping("queryBookType")
    public List<BookType> queryBookType(){
        return bookService.queryBookType();
    }

    /**
     * 通过id删除该图书
     * @param id
     * @return
     */
    @RequestMapping("delBookById")
    public boolean delBookById(int id){
        if (bookService.delBookById(id)>0){
            return true;
        }else {
            return false;
        }
    }

    /**
     * 通过id查询该图书的信息
     * @param id
     * @return
     */
    @RequestMapping("queryBookById")
    public Book queryBookById(int id){
        Book book=  bookService.queryBookById(id);
        return book;
    }

    /**
     * 通过id更新该图书
     * @param book
     * @return
     */
    @RequestMapping("updateBookById")
    public boolean updateBookById(Book book){
       if (bookService.updateBookById(book)>0){
           return true;
       }else{
           return false;
       }
    }

    /**
     * 分类获取书
     * @param bookType
     * @return
     */
    @RequestMapping("queryByType")
    public void queryBookByType(@RequestParam("pageNum") int pageNum,@RequestParam("bookType") int bookType, HttpServletRequest request, HttpServletResponse
                                response) {
        PageHelper.startPage(pageNum,10);
        List<Book> books = null;
        if(bookType == 0){
            books =bookService .queryBook();
        }else {
           books = bookService.queryBookByType(bookType);
        }
        PageInfo info = new PageInfo(books,5);

        request.getSession().setAttribute("page",info);
        request.getSession().setAttribute("bookType",bookType);
        try {
            response.sendRedirect(request.getContextPath()+"/index/booklist.jsp");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * 通过关键词查询图书，并分页
     * @param pn
     * @param ps
     * @return
     */
    @RequestMapping("admin_searchBook")
    public PageInfo admin_searchBook(@RequestParam(value = "pn", defaultValue = "1") Integer pn,
                             @RequestParam(value = "ps", defaultValue = "10") Integer ps,
                                     String searchBook){
        PageHelper.startPage(pn,ps);
        List<Book> books = bookService.queryBookBySearch(searchBook);
        PageInfo info = new PageInfo(books,5);
        return info;
    }

}
