package com.kgc.controller;


import com.kgc.entity.Book;
import com.kgc.entity.Order;
import com.kgc.service.BookService;
import com.kgc.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("order")
@ResponseBody
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private BookService bookService;

    /**
     * 查询全部订单
     * @param userId
     * @return
     */
    @RequestMapping("queryOrder")
    public List<Order> queryOrder(int userId){
        System.out.println(userId);
        return orderService.queryOrder(userId);
    }

    /**
     * 分类查询订单
     * @param userId
     * @param orderType
     * @return
     */
    @RequestMapping("queryOrderByType")
    public List<Order> queryOrderByType(int userId,int orderType){
        if(orderType==0){
            return orderService.queryOrder(userId);
        }
        return orderService.queryOrderByType(userId,orderType);
    }


    /**
     * 修该订单状态
     * @param orderType
     * @param orderNumber
     * @return
     */
    @RequestMapping("updataOrderType")
    public boolean updataOrderType(Integer orderType,String orderNumber){
        return orderService.updataOrderType(orderType,orderNumber) >= 1? true:false;
    }

    @RequestMapping("goAccount")
    public boolean goAccount(String orders, HttpServletRequest request, HttpServletResponse response) {
        String[] strs = orders.split(",");
        List<Book> orderBooks = new ArrayList<Book>();
        for (int i = 0; i < strs.length; i++) {
            if (i % 2 == 0) {
                int book_id = Integer.parseInt(strs[i]);
                orderBooks.add(bookService.queryBookById(book_id));
            } else {
                int orderBookNum = Integer.parseInt(strs[i]);
                orderBooks.get(i / 2).setNumber(orderBookNum);
            }
        }
        request.getSession().setAttribute("orderBooks", orderBooks);
        return true;
    }

    @RequestMapping("delOrder")
    public boolean delOrder(int id){
        return orderService.delOrder(id)>=1?true:false;
    }
}
