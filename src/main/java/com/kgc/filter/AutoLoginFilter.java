package com.kgc.filter;


import com.kgc.controller.UserController;
import com.kgc.entity.User;
import com.kgc.service.UserService;
import com.kgc.utils.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class AutoLoginFilter implements Filter {

    @Autowired
    UserService userService;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest res, ServletResponse req, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) res;
        HttpServletResponse response = (HttpServletResponse) req;
        HttpSession session = request.getSession();
        if(session.getAttribute("user") == null) {
            String value = CookieUtil.getCookie("auto", request);
            String tel = "";
            String password = "";
            if(value != null && value != ""){
                tel  = value.split("-")[0];
                password= value.split("-")[1];
            }


            User user = userService.queryByNameAndPassword(tel, password);

            if (user != null) {
                session.setAttribute("user", user);
        }
             chain.doFilter(res, req);

        }else {
            chain.doFilter(res, req);
        }

    }


    @Override
    public void destroy() {

    }
}

