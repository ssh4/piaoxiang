<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<link rel="stylesheet" href="../css/login.css" />
<script type="text/javascript" src="../js/loginmodal.js"></script>

<div class="modal fade " id="loginmodal">
    <!-- 自适应 -->
    <div class="modal-dialog modal-sm " style="margin-top: 200px; background-color: #F7F7F6;">
        <!--边框与阴影 -->
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">登录飘香账号</h4>
            </div>
            <div class="divider"></div>
            <div class="modal-body">
                <div class="from_div">
                    <div class="form-inline">

                        <div class="register-head">
                            <span><a id="got_c">短信验证码登录</a><a id="got_p"  style="display: none">手机号密码登录</a></span>
                            <div class="register">未有账号？
                                <a href="/piaoxiang/login_register/login_register.jsp">请注册</a>
                            </div>
                        </div>

                        <form id="t_plogin" action="#" method="get" >

                            <div><input type="text" id="tel" name="tel" maxlength="11" placeholder="手机号"/></div>

                            <div><input type="password" id="password" name="password" maxlength="12" placeholder="密码"/></div>
                            <div class="auto-div">
                                <div class="checkbox auto">
                                    <label>
                                        <input type="checkbox" id="autologin" name="autologin">下次自动登录
                                    </label>
                                </div>
                                <div class="forget" style="margin-top: 0px">
                                    <a href='javascript:window.parent.location.href="forgetpassword.jsp"'>忘记密码</a>
                                </div>
                                <div class="clearfix"></div>

                            </div>

                            <div>
                                <button type="button" id="submit">登录</button>
                            </div>

                        </form>
                        <form id="t_clogin" action="#" method="get" style="display: none">

                            <div><input type="text" id="ctel" name="ctel" maxlength="11" placeholder="手机号"/></div>

                            <div><input type="text" id="lcode" name="password" maxlength="12" placeholder="验证码"/><button type="button" id="locode">获取验证码</button></div>

                            <div>
                                <button type="button" id="csubmit" style="margin-top: 30px">登录</button>
                            </div>

                        </form>

                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
            <div class="divider"></div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
