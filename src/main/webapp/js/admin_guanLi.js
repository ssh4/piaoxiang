/*实时更新页面时间显示*/
setInterval("document.getElementById('cg').innerHTML=new Date().toLocaleString()",1000);
var pn = 1;
var ps = 7;
$(function(){
    /*图书管理和页面管理之间的切换*/
    $(".user a").click(function() {
        $(".userdiv").attr("style","display: block;");
        $(".bookdiv").attr("style","display: none;");
        window.location.reload();
    })
    $(".book a").click(function() {
        $(".bookdiv").attr("style","display: block;");
        $(".userdiv").attr("style","display: none;");
        window.location.reload();
    })
    /*分页默认值*/
    querybook(pn, ps);
    queryuser(pn,ps);
});
/*图书分页方法*/
function querybook(pn, ps) {
    $.getJSON("../book/selectAll",{"pn":pn, "ps":ps}, function(data){
        var obj = eval(data);
        console.log(data);
        var str = "";
        $(".bookTbody").empty();
        $(obj.list).each(function(){
            str += "<tr>"+
                "<td>"+this.id+"</td>"+
                "<td>"+this.name+"</td>"+
                "<td>"+this.author+"</td>"+
                "<td>"+this.publish+"</td>"+
                "<td>"+this.price+"</td>"+
                "<td>"+this.number+"</td>"+
                "<td>"+this.typeName.type+"</td>"+
                "<td>" +
                "<button  onclick=\"window.location.href='updatebook.jsp?id="+this.id+"'\" type=\"button\" class=\"btn btn-primary btn-sm\">" +
                "<span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>修改" +
                "</button>&nbsp;&nbsp;"+
                "<button  onclick='delbook("+this.id+",this)' type=\"button\" class=\"btn btn-danger btn-sm\">" +
                "<span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>删除" +
                "</button>"+
                "</tr>";
        });
        $(".bookTbody").append(str);

        var page = "";
        page += "<nav aria-label=\"Page navigation\">" +
            "<ul class=\"pagination\">" +
            "<li><a href=\"javascript:void(0)\" onclick='querybook(1,"+ps+")'>首页</a></li>";
        if(obj.hasPreviousPage){
            page += "<li>" +
                "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"querybook("+(obj.pageNum-1)+","+ps+")\">" +
                "<span aria-hidden=\"true\">&laquo;</span>" +
                "</a>" +
                "</li>";
        }

        $(obj.navigatepageNums).each(function () {
            if(obj.pageNum==this){
                page += "<li class='active'><a href=\"javascript:void(0)\" onclick='querybook("+this+","+ps+")'>"+this+"</a></li>";
            }else{
                page += "<li><a href=\"javascript:void(0)\" onclick='querybook("+this+","+ps+")'>"+this+"</a></li>";
            }

        })

        if(obj.hasNextPage){
            page += "<li>\n" +
                "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='querybook("+(obj.pageNum+1)+","+ps+")'>" +
                "<span aria-hidden=\"true\">&raquo;</span>" +
                "</a>" +
                "</li>";
        }
        page += "<li><a href=\"javascript:void(0)\" onclick='querybook("+obj.pages+","+ps+")'>未页</a></li>" +
            "</ul>" +
            "</nav>";
        $(".bookdiv nav").empty();
        $(".bookdiv nav").append(page);

    });
}
/*增加图书的页面跳转*/
function addbook() {
    window.location.href="addbook.jsp";
}
/*通过id删除该条图书记录*/
function delbook(id,obj){
    if(confirm("确认删除吗?")){
        $.getJSON("../book/delBookById",{"id":id},function(data){
            if(data){
                alert("删除成功!");
                $(obj).parents("tr").remove();
            }else{
                alert("删除失败!");
            }
        });
    }
}

/*用户分页方法*/
function queryuser(pn,ps) {
    $.getJSON("../user/selectAll",{"pn":pn, "ps":ps}, function(data){
        var obj = eval(data);
        var str = "";
        $("#userTbody").empty();
        $(obj.list).each(function(){
            str += "<tr>"+
                "<td>"+this.id+"</td>"+
                "<td>"+this.name+"</td>"+
                "<td>"+this.password+"</td>"+
                "<td>"+this.age+"</td>"+
                "<td>"+this.sex+"</td>"+
                "<td>"+this.userRole.roleName+"</td>"+
                "<td>"+this.tel+"</td>"+
                "<td>"+this.birthday+"</td>"+
                "<td>" +
                "<button  onclick=\"window.location.href='updateuser.jsp?id="+this.id+"'\" type=\"button\" class=\"btn btn-primary btn-sm\">" +
                "<span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>修改" +
                "</button>&nbsp;&nbsp;"+
                "<button  onclick='deluser("+this.id+",this)' type=\"button\" class=\"btn btn-danger btn-sm\">" +
                "<span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>删除" +
                "</button>"+
                "</tr>";
        });
        $("#userTbody").append(str);
        var page = "";
        page += "<nav aria-label=\"Page navigation\">" +
            "<ul class=\"pagination\">" +
            "<li><a href=\"javascript:void(0)\" onclick='queryuser(1,"+ps+")'>首页</a></li>";
        if(obj.hasPreviousPage){
            page += "<li>" +
                "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"queryuser("+(obj.pageNum-1)+","+ps+")\">" +
                "<span aria-hidden=\"true\">&laquo;</span>" +
                "</a>" +
                "</li>";
        }

        $(obj.navigatepageNums).each(function () {
            if(obj.pageNum==this){
                page += "<li class='active'><a href=\"javascript:void(0)\" onclick='queryuser("+this+","+ps+")'>"+this+"</a></li>";
            }else{
                page += "<li><a href=\"javascript:void(0)\" onclick='queryuser("+this+","+ps+")'>"+this+"</a></li>";
            }

        })

        if(obj.hasNextPage){
            page += "<li>\n" +
                "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='queryuser("+(obj.pageNum+1)+","+ps+")'>" +
                "<span aria-hidden=\"true\">&raquo;</span>" +
                "</a>" +
                "</li>";
        }
        page += "<li><a href=\"javascript:void(0)\" onclick='queryuser("+obj.pages+","+ps+")'>未页</a></li>" +
            "</ul>" +
            "</nav>";
        $(".userdiv nav").empty();
        $(".userdiv nav").append(page);
    });
}
/*增加用户页面跳转*/
function adduser() {
    window.location.href="adduser.jsp";
}
/*通过id删除该条用户*/
function deluser(id,obj){
    if(confirm("确认删除吗?")){
        $.getJSON("../user/delUserById",{"id":id},function(data){
            if(data){
                alert("删除成功!");
                $(obj).parents("tr").remove();
            }else{
                alert("删除失败!");
            }
        });
    }
}

/*图书搜索方法*/
function searchBook(pn,ps){
    var searchBook = document.getElementById("searchBook").value;
    $.getJSON("../book/admin_searchBook",{"pn":pn,"ps":ps,"searchBook":searchBook},function (data) {
        if (data.firstPage == 0){
            alert("对不起找不到您要搜索的图书信息!");
        }else {
            var obj = eval(data);
            var str = "";
            $(".bookTbody").empty();
            $(obj.list).each(function(){
                str += "<tr>"+
                    "<td>"+this.id+"</td>"+
                    "<td>"+this.name+"</td>"+
                    "<td>"+this.author+"</td>"+
                    "<td>"+this.publish+"</td>"+
                    "<td>"+this.price+"</td>"+
                    "<td>"+this.number+"</td>"+
                    "<td>"+this.typeName.type+"</td>"+
                    "<td>" +
                    "<button  onclick=\"window.location.href='updatebook.jsp?id="+this.id+"'\" type=\"button\" class=\"btn btn-primary btn-sm\">" +
                    "<span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>修改" +
                    "</button>&nbsp;&nbsp;"+
                    "<button  onclick='delbook("+this.id+",this)' type=\"button\" class=\"btn btn-danger btn-sm\">" +
                    "<span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>删除" +
                    "</button>"+
                    "</tr>";
            });
            $(".bookTbody").append(str);

            var page = "";
            page += "<nav aria-label=\"Page navigation\">" +
                "<ul class=\"pagination\">" +
                "<li><a href=\"javascript:void(0)\" onclick='searchBook(1,"+ps+")'>首页</a></li>";
            if(obj.hasPreviousPage){
                page += "<li>" +
                    "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"searchBook("+(obj.pageNum-1)+","+ps+")\">" +
                    "<span aria-hidden=\"true\">&laquo;</span>" +
                    "</a>" +
                    "</li>";
            }

            $(obj.navigatepageNums).each(function () {
                if(obj.pageNum==this){
                    page += "<li class='active'><a href=\"javascript:void(0)\" onclick='searchBook("+this+","+ps+")'>"+this+"</a></li>";
                }else{
                    page += "<li><a href=\"javascript:void(0)\" onclick='searchBook("+this+","+ps+")'>"+this+"</a></li>";
                }

            })

            if(obj.hasNextPage){
                page += "<li>\n" +
                    "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='searchBook("+(obj.pageNum+1)+","+ps+")'>" +
                    "<span aria-hidden=\"true\">&raquo;</span>" +
                    "</a>" +
                    "</li>";
            }
            page += "<li><a href=\"javascript:void(0)\" onclick='searchBook("+obj.pages+","+ps+")'>未页</a></li>" +
                "</ul>" +
                "</nav>";
            $(".bookdiv nav").empty();
            $(".bookdiv nav").append(page);
        }
    })
}
/*用户搜索方法*/
function searchUser(pn,ps) {
    var searchUser = document.getElementById("searchUser").value;
    $.getJSON("../user/adminsearchUser",{"pn":pn, "ps":ps,"searchUser":searchUser}, function(data){
        if (data.firstPage == 0){
            alert("对不起找不到您要搜索的用户信息!");
        }else {
            var obj = eval(data);
            var str = "";
            $("#userTbody").empty();
            $(obj.list).each(function () {
                str += "<tr>" +
                    "<td>" + this.id + "</td>" +
                    "<td>" + this.name + "</td>" +
                    "<td>" + this.password + "</td>" +
                    "<td>" + this.age + "</td>" +
                    "<td>" + this.sex + "</td>" +
                    "<td>" + this.userRole.roleName + "</td>" +
                    "<td>" + this.tel + "</td>" +
                    "<td>" + this.birthday + "</td>" +
                    "<td>" +
                    "<button  onclick=\"window.location.href='updateuser.jsp?id=" + this.id + "'\" type=\"button\" class=\"btn btn-primary btn-sm\">" +
                    "<span class=\"glyphicon glyphicon-pencil\" aria-hidden=\"true\"></span>修改" +
                    "</button>&nbsp;&nbsp;" +
                    "<button  onclick='deluser(" + this.id + ",this)' type=\"button\" class=\"btn btn-danger btn-sm\">" +
                    "<span class=\"glyphicon glyphicon-trash\" aria-hidden=\"true\"></span>删除" +
                    "</button>" +
                    "</tr>";
            });
            $("#userTbody").append(str);
            var page = "";
            page += "<nav aria-label=\"Page navigation\">" +
                "<ul class=\"pagination\">" +
                "<li><a href=\"javascript:void(0)\" onclick='searchUser(1," + ps + ")'>首页</a></li>";
            if (obj.hasPreviousPage) {
                page += "<li>" +
                    "<a href=\"javascript:void(0)\" aria-label=\"Previous\" onclick=\"searchUser(" + (obj.pageNum - 1) + "," + ps + ")\">" +
                    "<span aria-hidden=\"true\">&laquo;</span>" +
                    "</a>" +
                    "</li>";
            }

            $(obj.navigatepageNums).each(function () {
                if (obj.pageNum == this) {
                    page += "<li class='active'><a href=\"javascript:void(0)\" onclick='searchUser(" + this + "," + ps + ")'>" + this + "</a></li>";
                } else {
                    page += "<li><a href=\"javascript:void(0)\" onclick='searchUser(" + this + "," + ps + ")'>" + this + "</a></li>";
                }

            })

            if (obj.hasNextPage) {
                page += "<li>\n" +
                    "<a href=\"javascript:void(0)\" aria-label=\"Next\" onclick='searchUser(" + (obj.pageNum + 1) + "," + ps + ")'>" +
                    "<span aria-hidden=\"true\">&raquo;</span>" +
                    "</a>" +
                    "</li>";
            }
            page += "<li><a href=\"javascript:void(0)\" onclick='searchUser(" + obj.pages + "," + ps + ")'>未页</a></li>" +
                "</ul>" +
                "</nav>";
            $(".userdiv nav").empty();
            $(".userdiv nav").append(page);
        }
    });
}