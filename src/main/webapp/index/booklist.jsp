<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>

<head>
    <meta charset="UTF-8">
    <title>图书详情</title>
    <link rel="icon" href="../img/favicon.ico" mce_href="../img/favicon.ico" type="image/x-icon">
    <link rel="stylesheet" href="../css/bootstrap.css"/>
    <link rel="stylesheet" href="../css/booklist.css"/>
    <link rel="stylesheet" href="../css/public_top.css"/>

    <script type="text/javascript" src="../js/jquery-1.12.4.js"></script>
    <script type="text/javascript" src="../js/bootstrap.js"></script>
    <script>
        $(function () {
            $.getJSON("../book/queryBookType", function (data) {
                var type  = "${bookType }"
                var str = "";
                var flag = false;
                $(data).each(function () {
                    if(this.id == type) {
                        str += "<li><a style='color: darkred'  href=\"../book/queryByType?pageNum=1&bookType=" + this.id + "\">" + this.type + "</a></li>";
                        flag =true;
                    }else {
                        str += "<li><a href=\"../book/queryByType?pageNum=1&bookType=" + this.id + "\">" + this.type + "</a></li>";
                    }
                })
                if(!flag){
                      $("#type0").css("color","darkred") ;
                }
                $("#bookTypeName").append(str);

            })
        })
    </script>
</head>

<body>
<jsp:include page="../public/public_top.jsp"></jsp:include>
<div class="body">

    <div>
        <ul class="breadcrumb" id="bookTypeName">
            <li>
                <a id="type0" href="../book/queryByType?pageNum=1&bookType=0">全部</a>
            </li>

        </ul>
    </div>
    <div class="list-title">
        <span style="margin-right:  50px;font-size: 12px;float: right;">共${page.total}&nbsp;本</span>
    </div>
    <div class="book-all">
        <c:forEach items="${page.list}" var="book">


            <br/>
            <div class="row">
                <div class="book-only book-first">
                    <div class="col-md-2">
                        <img src="${book.picture}"/>
                    </div>
                    <div class="col-md-8">
                        <ul class="book-ul-a">
                            <li>
                                <a href="bookInfo.jsp?id=${book.id}&number=&${book.number}">${book.name}</a>
                            </li>
                            <li>
                                    ${book.author} 著 / ${book.publish} / ${book.publishDate}
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <ul class="book-ul-b">
                            <li>价格：<span>${book.price}￥</span></li>
                            <li>库存：<span>${book.number}</span>本</li>
                        </ul>

                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>


            <br/>

        </c:forEach>

        <c:if test="${page.pages >1}">

            <div class="row text-center">
                <nav aria-label="Page navigation">
                    <ul class="pagination">
                        <li><a href="javascript:void(0)"
                               onclick="window.location.href='../book/queryByType?pageNum=1&bookType=${bookType }'">首页</a>
                        </li>
                        <c:forEach items="${page.navigatepageNums }" var="apage">
                            <li
                                    <c:if test="${page.pageNum == apage}">class="active"</c:if> ><a
                                    href="javascript:void(0)"
                                    onclick="window.location.href='../book/queryByType?pageNum=${apage }&bookType=${bookType }'">${apage }</a>
                            </li>

                        </c:forEach>

                        <c:if test="${page.hasNextPage}">
                            <li>
                                <a href="javascript:void(0)" aria-label="Next"
                                   onclick="window.location.href='../book/queryByType?pageNum=${page.navigateLastPage}&bookType=${bookType }'"><span
                                        aria-hidden="true">»</span></a></li>
                        </c:if>
                        <li><a href="javascript:void(0)" onclick="window.location.href='../book/queryByType?pageNum=${page.pages }&bookType=${bookType }'">末页</a></li>
                    </ul>
                </nav>
            </div>
        </c:if>


    </div>


</div>

</body>
</html>